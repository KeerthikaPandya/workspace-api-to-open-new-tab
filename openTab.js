{(
    openTab : function(component, event, helper) {
        var workspaceAPI = component.find("workspace");
        workspaceAPI.openTab({
            url: '#/sObject/001R0000003HgssIAC/view',
            focus: true
        });
    },
)}